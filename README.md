# Double Heat Sink baseCoolingRate Fix

Corrects the base cooling rate for mechs with double heat sinks

* Includes mechs from Mechwarrior 5
* Includes mechs from Heroes of the Inner Sphere DLC
* Includes mechs from Legend of the Kestrel Lancers DLC

## Background

Mechs in Mechwarrior / Battletech with an engine rating of 250 or higher have 10 built in heat sinks. In Mechwarrior 5, each single heat sink (SHS) contribute 0.1 to the mech's heat dissipation. Each double heat sink (DHS) contributes 0.2. This means most mechs with SHS should have a baseCoolingRate of 1 (0.1 x 10) and most mechs with DHS should have a baseCoolingRate of 2 (0.2 x 10). For some reason in Mechwarrior 5, all mechs have a baseCoolingRate of 1 or less. This mod doubles the baseCoolingRate of mechs with DHS from 0.1 per heat sink to 0.2, which significantly increases the mech's effectiveness.

## Using this mod in Mechwarrior 5

To use this mod download it from [Nexus Mods](https://www.nexusmods.com/mechwarrior5mercenaries/mods/371).

## Using these files in your own mod

Feel free to use these files in your own Mechwarrior 5 mods! Please give credit to the author `moddingisthegame`.

1. Purchase [Mechwarrior 5 for PC](https://www.epicgames.com/store/en-US/p/mechwarrior-5)
2. Install the [Mechwarrior 5 Modding Toolkt](https://www.epicgames.com/store/en-US/p/mechwarrior-5--mod-editor)
3. Install [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
4. Navigate to the Mechwarrior 5 Editor Plugins directory (typically C:\Program Files\Epic Games\MechWarrior5Editor\MW5Mercs\Plugins) in your terminal of choice
5. `git clone https://gitlab.com/mechwarrior5mods/bug-fixes/dhsbasecoolingratefixv2.git`
6. Launch the Mechwarrior 5 Modding Toolkit.
7. Click the dropdown next to Manage Mod and select `dhsbasecoolingratefixv2`
8. Mod away!

## Compatibility

* This mod overrides the MDA (mech data) files for all mechs with double heat sinks.

## Similar Mods

If you'd like a mod that increases both baseCoolingRate and baseHeatCapacity of DHS, check out [SeriousHeatSinkFix](https://www.nexusmods.com/mechwarrior5mercenaries/mods/313?tab=files&file_id=2058)
